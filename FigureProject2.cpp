// PROJEKT 1 "Figura"
// Nr. Zadania 115
// Mateusz Gruszka grupa 5
// PJATK

#include <iostream>						// 
#include <conio.h>						//	potrzebne biblioteki do wykonania projektu
#include <Windows.h>					//

using namespace std;

void gotoxy(int x, int y);
void HideCursor();
void operowanieFigury(char symbol, int size);
void menu(char &symbol, int &size);

const int Esc = 27;						
char symbol;
int siize = 0;
int x, y;

int main()
{
	HANDLE uchwyt;
	uchwyt = GetStdHandle(STD_OUTPUT_HANDLE);	// warto�ci� zwr�con� b�dzie uchwyt do standardowego wyjscia
	SetConsoleTextAttribute(uchwyt, 11);		// ustawiamy atrybuty, 11- odpowiada za kolor blekitny

	menu(symbol, siize);
	operowanieFigury(symbol, siize);
	return 0;
}
//----------------------------------------------------------------------------
void gotoxy(int x, int y) {
	COORD c;														// tworzymy koordynaty 
	c.X = x;														// przypisujemy wartosc x dla koordynatow X'ych
	c.Y = y;														// przypisujemy wartosc y dla koordynatow Y'ych
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);   // ustawiamy pozycje kursora dla okreslonych wspolrzednych w argumencie funkcji
}
//----------------------------------------------------------------------------
void HideCursor() {
	HANDLE hConsoleOut = GetStdHandle(STD_OUTPUT_HANDLE);    // tworzymy uchwyt dla standardowego wyjscia
	CONSOLE_CURSOR_INFO hCCI;								 // tworzymy zmienna do przechowania informacji o kursorze
	GetConsoleCursorInfo(hConsoleOut, &hCCI);				 // pozyskuje informacje o kursorze (jego rozmiar i widocznosc) do zmiennej 
	hCCI.bVisible = FALSE;									 // zmieniamy widocznosc na FALSE (niewidoczny)
	SetConsoleCursorInfo(hConsoleOut, &hCCI);				 // ustawiamy preferowana widocznosc 
}
//-----------------------------------------------------------------------------
void operowanieFigury(char symbol, int size) {

	int  x1, y1, i, j, q, w;
	char klawisz,
	x = 40; y = 10; 			// wspolrzedne, w ktorych zostaje rysowana figura
	
	int columns, rows;															
	CONSOLE_SCREEN_BUFFER_INFO csbi;											// zmienna ktora bedzie przechowywac informacje
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);			// zapisujemy informacje do zmiennej
	columns = csbi.srWindow.Right - csbi.srWindow.Left + 1;						// dlugosc w poziomie (ostatnia kolumna okna - pierwsza kolumna okna) 
	rows = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;							// dlugosc w pionie (dolny rz�d okna - gorny rz�d okna)

	do
	{
		system("cls");		// czyscimy konsole
		x1 = x; y1 = y;		// punkt zaczepienia
		gotoxy(x1, y1);
		for (q = 0; q<siize; q++) {
			for (w = 0; w<siize; w++) {	// rysowanie dolnego odcinka figury
				if ((q + w) == siize) {
					gotoxy(x + q - 1, siize + y + w - 1);
					cout << symbol;
				}
			}}
		for (i = 0; i<siize; i++) {	// rysowanie gornego odcinka figury
			for (j = 0; j<siize; j++) {
				if (i == j) {
					gotoxy(x + i, y + j);
					cout << symbol;
				}
			}}
		HideCursor();
		klawisz = _getch();
		switch (klawisz)
		{
		case '+': {				// powiekszanie figury
			if ((y < rows - (2 * siize)) && (x + size < 79))
				siize = siize + 1;
			break; }
		case '-': {				// pomniejszanie figury
			if (siize>2)
				siize = siize - 1;
			break; }
		case 75: {				// przesuniecie figury w lewo
			if (x>0) x--;
			break; }
		case 77: {				// przesuni�cie figury w prawo
			if (x + size<columns) x++;
			break; }
		case 72: {				// przesuni�cie figury w g�r�
			if (y>0)	y--;
			break; }
		case 80: {				// przesuniecie figury w d�
			if (y<rows - (2 * size))	 y++;
			break; }
		case Esc: { break; }
		}

	} while (klawisz != Esc);  // wyj�cie z programu za pomoca klawisza ESC
}
//-------------------------------------------------------------------
void menu(char &symboll, int &sizee) {

	cout << " OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOO                                                    OOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOO                  PROJEKT-FIGURA                    OOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOO                        >                           OOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOO                                                    OOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOO Aby zmienic polozenie figury korzystaj ze strzalek OOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOO                                                    OOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOO    Aby zmienic rozmiar, korzystaj z przyciwskow    OOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOO                     +  /  -                        OOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOO          Aby wyjsc wscisnij klawisz ESC            OOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOO                                                    OOOOOOOOOOO" << endl;
	cout << " OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO" << endl;

	cout << "                           Podaj wielkosc figury:  " << endl;
	cin >> sizee;

	while (sizee > 13){		//sprawdzenie poprwanosci wprowadzonego rozmiaru

		cout << " BLAD: Zostala wprowadzona zla wartosc wielkosci figury. Podaj wartosc jeszcze raz. " << endl;
		cin >> sizee;
	}
	cout << "              Podaj znak z ktorego bedzie skladac sie figura:   " << endl;
	cin >> symbol;
}